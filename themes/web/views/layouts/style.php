<style>
    .carousel-caption p {
        /* margin-bottom: 20px; */
        font-size: 21px;
        line-height: 1.4;
    }

    .section-event {
        text-align: justify;
    }

    .pengunjung-online {
        display: inline-block;
        background: #1cb71c;
        padding-top: 8px;
        padding-bottom: 8px;
        padding-left: 10px;
        padding-right: 10px;
        border-radius: 6px;
    }

    .pengunjung-online-detail {
        display: inline-block;
        background: #cb1919;
        padding-left: 10px;
        padding-right: 10px;
        border-radius: 6px;
    }

    .content-news {
        background-color: #FAB60C; 
        text-align: center;
        padding: 10px;
        border-bottom-left-radius: 15px;
        border-bottom-right-radius: 15px;
    }

    .content-image-news {
        border-style: solid;
        border-color: #FAB60C;
        background-color: #FAB60C;
    }
    #instafeed-container > div img{ 
        width: 100%;
        padding: 4px;
    }

    #myVideo {
        height: 100%; 
        width: 100%;
        object-fit: cover;
        max-height: 630px;
    }

    .instagram-span-caption {
        overflow: hidden;
        white-space: nowrap;
        text-overflow: ellipsis;
        max-width: 100px;
        font-size: 8px;
        margin-bottom: 0rem!important;
    }

    .item-instagram {
        width:33%;
        text-align:center;
        display:block;
        background-color: transparent;
        border: 1px solid transparent;
        float:left;
    }

    .floating{
        position:fixed;
        width:60px;
        height:60px;
        bottom:40px;
        left:40px;
        background-color:#25d366;
        color:#FFF;
        border-radius:50px;
        text-align:center;
        font-size:30px;
        box-shadow: 2px 2px 3px #999;
    z-index:100;
    }

    .float-button{
        margin-top:16px;
    }
</style>
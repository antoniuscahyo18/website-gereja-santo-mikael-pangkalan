<footer class="ftco-footer">
    <div class="container">
        <div class="row">
            <div class="col-md" style="padding-top: 50px;">
                <div class="ftco-footer-widget mb-8">
                    <img src="<?php echo Yii::app()->getBaseUrl(1) ?>/themes/web/images/logo_pangkalan.png" height="50px">
                    <div class="block-23 mb-3" style="padding-top: 20px;">
                        <ul>
                            <li><span class="icon icon-map-marker"></span><span class="text">Pangkalan TNI AU, Jl. Lettu TPT Sapardal Karang Jambe, Kec. Banguntapan Kabupaten Bantul, Daerah Istimewa Yogyakarta 55281</span></li>
                            <li><a href="#"><span class="icon icon-phone"></span><span class="text">+2 392 3929 210</span></a></li>
                            <li><a href="#"><span class="icon icon-envelope"></span><span class="text">parokipangkalan@gmail.com</span></a></li>
                        </ul>
                    </div>
                    <ul class="ftco-footer-social list-unstyled float-md-left float-lft mt-5">
                        <li class="ftco-animate"><a href="https://web.facebook.com/gerejapangkalanyk"><span class="icon-facebook"></span></a></li>
                        <li class="ftco-animate"><a href="https://www.instagram.com/gerejapangkalan"><span class="icon-instagram"></span></a></li>
                        <li class="ftco-animate"><a href="https://www.youtube.com/channel/UCm529B0TWYxGKPuRn35tYJg"><span class="icon-youtube"></span></a></li>
                    </ul>
                </div>
            </div>
            <div class="col-md" style="padding-top: 50px;">
                <div class="ftco-footer-widget mb-4">
                    <h2 class="ftco-heading-2">Lingkungan</h2>
                    <ul class="list-unstyled">
                        <?php foreach(Reflingkungan::model()->findAll() as $dtlingk): ?>
                            <li><a href="javascript:void(0);"><span class="icon-radio_button_unchecked mr-2"></span><?= $dtlingk['reflingkungan_nama']; ?></a></li>
                        <?php endforeach; ?>
                    </ul>
                </div>
            </div>
            <?php Yii::app()->userCounter->refresh(); ?>
            <div class="col-md" style="padding-top: 50px;color:white;">
                <div class="ftco-footer-widget mb-4">
                    <h2 class="ftco-heading-2">Statistik Pengunjung</h2>
                    <ul class="list-unstyled">
                        <li>
                            <div class="pengunjung-online">
                                <span class="text">Pengunjung Online : </span>
                                <div class="pengunjung-online-detail"><?php echo Yii::app()->userCounter->getOnline(); ?></div>
                            </div>
                        </li>
                        <li><span class="text">Hari Ini : </span><?php echo Yii::app()->userCounter->getToday(); ?></li>
                        <li><span class="text">Kemarin : </span><?php echo Yii::app()->userCounter->getYesterday(); ?></li>
                        <li><span class="text">Total : </span><?php echo Yii::app()->userCounter->getTotal(); ?></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 text-center" style="background-color: #FAB60C;border-top-left-radius: 25px;border-top-right-radius: 25px;padding-top: 15px;">
                <p style="color: black;">
                    Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | Gereja Katolik Paroki Santo Mikael Pangkalan</a>
                </p>
            </div>
        </div>
    </div>
</footer>
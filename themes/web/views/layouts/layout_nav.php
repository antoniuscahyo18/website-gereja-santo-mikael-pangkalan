<!-- <div style="background-color:#FAB60C;text-align:center;">
    <div class="ftco-footer-social" style="padding-top: 8px;">
        <li class="ftco-animate"><a href="https://web.facebook.com/gerejapangkalanyk"><span class="icon-facebook"></span></a></li>
        <li class="ftco-animate"><a href="https://www.instagram.com/gerejapangkalan"><span class="icon-instagram"></span></a></li>
        <li class="ftco-animate"><a href="https://www.youtube.com/channel/UCm529B0TWYxGKPuRn35tYJg"><span class="icon-youtube"></span></a></li>
    </div>
</div> -->
<nav class="navbar navbar-expand-lg navbar-light ftco_navbar bg-light ftco-navbar-light site-navbar-target" id="ftco-navbar">
    <div class="container">
        <a class="navbar-brand" href="#" width="50%">
            <img src="<?php echo Yii::app()->getBaseUrl(1) ?>/themes/web/images/logo_pangkalan.png" class="img-logo-header">
        </a>
        <button class="navbar-toggler js-fh5co-nav-toggle fh5co-nav-toggle" type="button" data-toggle="collapse" data-target="#ftco-nav" aria-controls="ftco-nav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="oi oi-menu"></span>
        </button>
        
        <div class="collapse navbar-collapse" id="ftco-nav">
            <ul class="navbar-nav nav ml-auto">
                <?php foreach(Webmenu::model()->findAll('tblwebmenu_status=:sts', array(':sts'=>'T')) as $menu): ?>
                <?php if (strpos($menu['tblwebmenu_link'], 'section')): ?>
                    <li class="nav-item"><a href="<?php echo Yii::app()->getBaseUrl() ?>/#<?= $menu['tblwebmenu_link']; ?>" class="nav-link"><span><?= $menu['tblwebmenu_nama']; ?></span></a></li>
                <?php else: ?>
                    <li class="nav-item"><a href="<?php echo Yii::app()->getBaseUrl() ?>/web/<?= $menu['tblwebmenu_link']; ?>" class="nav-link"><span><?= $menu['tblwebmenu_nama']; ?></span></a></li>
                <?php endif ?>
                <?php endforeach; ?>
            </ul>
        </div>
    </div>
</nav>
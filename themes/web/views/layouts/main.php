<!DOCTYPE html>
<html lang="en">
<head>
	<title><?php echo CHtml::encode($this->pageTitle); ?></title>
	<link rel="shortcut icon" href="<?php echo Yii::app()->request->baseUrl; ?>/images/favicon.png" type="image/x-icon">
		<link rel="icon" href="<?php echo Yii::app()->request->baseUrl; ?>/images/favicon.png" type="image/x-icon">
		
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	
	<link href="https://fonts.googleapis.com/css?family=Poppins:100,200,300,400,500,600,700,800,900" rel="stylesheet">
	
	<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/themes/web/css/open-iconic-bootstrap.min.css">
	<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/themes/web/css/animate.css">
	
	<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/themes/web/css/owl.carousel.min.css">
	<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/themes/web/css/owl.theme.default.min.css">
	<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/themes/web/css/magnific-popup.css">
	
	<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/themes/web/css/aos.css">
	
	<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/themes/web/css/ionicons.min.css">
	
	<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/themes/web/css/flaticon.css">
	<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/themes/web/css/icomoon.css">
	<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/themes/web/css/style.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
</head>

<body data-spy="scroll" data-target=".site-navbar-target" data-offset="300">
	<a href="https://api.whatsapp.com/send?phone=6285701199300" class="floating" target="_blank">
		<i class="fa fa-whatsapp float-button"></i>
	</a>
	
    <?php include 'layout_nav.php'; ?>

    <?php echo $content; ?>

    <?php include 'layout_footer.php'; ?>
	
	<!-- loader -->
	<div id="ftco-loader" class="show fullscreen"><svg class="circular" width="48px" height="48px"><circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee"/><circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10" stroke="#F96D00"/></svg></div>
	
    <?php include 'script.php'; ?>
    <?php include 'style.php'; ?>
	
</body>
</html>

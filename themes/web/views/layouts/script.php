<script src="https://cdn.jsdelivr.net/gh/stevenschobert/instafeed.js@2.0.0rc1/src/instafeed.min.js"></script>
<script type="text/javascript">
	var userFeed = new Instafeed({
		get: 'user',
		target: "instafeed-container",
    	resolution: 'low_resolution',
		limit: 9,
		template: '<div class="item-instagram" onclick="redirectInstagram(\'{{link}}\')" style="cursor: pointer;"><img title="{{caption}}" src="{{image}}" /><p class="instagram-span-caption">{{caption}}</p></div>',
		accessToken: 'IGQVJWM25jMTI4X0hCRHV3aVhrU2VIcUtsT1pMZADFkOWZAvbVZA4NVU3aVhHb2JmYW42SG1ZAcDNlVzZAzOE1CYThhWXNFVUs2bDRMVG44WXl1M0prWHY5aUt5OWM5cTIzcFp0N0Y2U2tyWm0xWjZA5dmEtSwZDZD'
	});
	userFeed.run();
	
	function redirectInstagram(url) {
		window.location = url;
	}
</script>
<script src="<?php echo Yii::app()->getBaseUrl(1) ?>/themes/web/js/jquery.min.js"></script>
<script src="<?php echo Yii::app()->getBaseUrl(1) ?>/themes/web/js/jquery-migrate-3.0.1.min.js"></script>
<script src="<?php echo Yii::app()->getBaseUrl(1) ?>/themes/web/js/popper.min.js"></script>
<script src="<?php echo Yii::app()->getBaseUrl(1) ?>/themes/web/js/bootstrap.min.js"></script>
<script src="<?php echo Yii::app()->getBaseUrl(1) ?>/themes/web/js/jquery.easing.1.3.js"></script>
<script src="<?php echo Yii::app()->getBaseUrl(1) ?>/themes/web/js/jquery.waypoints.min.js"></script>
<script src="<?php echo Yii::app()->getBaseUrl(1) ?>/themes/web/js/jquery.stellar.min.js"></script>
<script src="<?php echo Yii::app()->getBaseUrl(1) ?>/themes/web/js/owl.carousel.min.js"></script>
<script src="<?php echo Yii::app()->getBaseUrl(1) ?>/themes/web/js/jquery.magnific-popup.min.js"></script>
<script src="<?php echo Yii::app()->getBaseUrl(1) ?>/themes/web/js/aos.js"></script>
<script src="<?php echo Yii::app()->getBaseUrl(1) ?>/themes/web/js/jquery.animateNumber.min.js"></script>
<script src="<?php echo Yii::app()->getBaseUrl(1) ?>/themes/web/js/scrollax.min.js"></script>
<script src="<?php echo Yii::app()->getBaseUrl(1) ?>/themes/web/js/main.js"></script>
<!-- Embed Youtube Channel -->
<script>
	var reqURL = "https://api.rss2json.com/v1/api.json?rss_url=" + encodeURIComponent("https://www.youtube.com/feeds/videos.xml?channel_id=");

	function loadVideo(iframe) {
	$.getJSON(reqURL + iframe.getAttribute('cid'),
		function(data) {
		var videoNumber = (iframe.getAttribute('vnum') ? Number(iframe.getAttribute('vnum')) : 0);
		console.log(videoNumber);
		var link = data.items[videoNumber].link;
		var title=data.items[videoNumber].title;
		id = link.substr(link.indexOf("=") + 1);
		iframe.setAttribute("src", "https://youtube.com/embed/" + id + "?controls=0&autoplay=1");
		iframe.parentElement.querySelector("#video-title").innerText = title;
		}
	);
	}

	var iframes = document.getElementsByClassName('latestVideoEmbed');
	for (var i = 0, len = iframes.length; i < len; i++) {
	loadVideo(iframes[i]);
	}

</script>
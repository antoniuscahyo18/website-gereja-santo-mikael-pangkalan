<?php

class WebController extends Controller
{

	public function init()
    {
        Yii::app()->theme = 'web'; // memanggil tema web saat controller nya web
        Yii::app()->userCounter->refresh();
    }

	public $berita;
	public $pages;
	public $webmenu_parent;
	public $webconfig;
	public $modemenu;
	public $idmodul;
	public $namamodul;

	public function actionIndex()
	{	
		Yii::import('ext.TanggalIndonesia');

		$this->modemenu       = Yii::app()->request->getParam('dummy');
		$data['sambutan']     = WebKontent::model()->find('tblwebkontent_mode=:kat', array(':kat'=>'SAMBUTAN'));
		$this->webmenu_parent = Webmenu::model()->findAll('tblwebmenu_status=:stat AND tblwebmenu_parent=:parent', array(':stat'=>'T',':parent'=>0));
		$this->webconfig      = WebConfig::model()->find();

		//BERITA
		$model               = new WebKontent();
		$criteria            = new CDbCriteria;
		$criteria->condition = 'tblwebkontent_mode=:kat AND tblwebkontent_status=:stat';
		$criteria->limit     = 2;
		$criteria->params    = array(':kat'=>'BERITA', ':stat'=>'T');
		$criteria->order     = 'tblwebkontent_sysinsert DESC';
		$total               = $model->count($criteria);
		$pages               = new CPagination($total);
		$pages->pageSize     = 5;
		$pages->applyLimit($criteria);
		// $data['news'] = WebKontent::model()->findAll($criteria);

		$data['news'] = WebKontent::model()->findAll(array(
			'condition' => 'tblwebkontent_mode=:kat AND tblwebkontent_tampilhome=:stat',
			'limit' => 4,
			'params' => array(':kat'=>'NEWS', ':stat'=>'T'),
			'order' => 'tblwebkontent_sysinsert DESC',
		));

		$data['event'] = WebKontent::model()->findAll(array(
			'condition' => 'tblwebkontent_mode=:kat AND tblwebkontent_tampilhome=:stat',
			'limit' => 4,
			'params' => array(':kat'=>'EVENT', ':stat'=>'T'),
			'order' => 'tblwebkontent_sysinsert DESC',
		));

		$this->render('index', array('data'=>$data, 'pages'=>$pages));
	}

	public function actionPage()
	{		
		$this->webconfig = WebConfig::model()->find();
		$idmenu          = Yii::app()->request->getParam('id');
		$idmodul         = Webmenu::model()->findByPk($idmenu)->tblwebmodul_id;
		$model_modul     = WebModul::model()->findByPk($idmodul);
		$this->namamodul = $model_modul['tblwebmodul_file'];

		$this->render('page');
	}

	public function actionKontent()
	{
		$this->webconfig = WebConfig::model()->find();
		$this->modemenu  = $this->action->id; 
		$id              = Yii::app()->request->getParam('id');
		$model_kontent   = WebKontent::model()->find('tblwebmenu_id=:idmenu AND tblwebkontent_status=:stat', array(':idmenu'=>$id, ':stat'=>'T'));
		//$this->modemenu = Yii::app()->request->getParam('dummy');
		$this->render('kontent', array('model_kontent' =>$model_kontent));
	}

	public function actionNews()
	{
		Yii::import('ext.TanggalIndonesia');
		$id = Yii::app()->request->getParam('id');

		$dataKontent = WebKontent::model()->findByPk($id);

		$this->render('detail_news', array(
				'dataKontent' =>$dataKontent
			)
		);
	}

	public function actionEvent()
	{
		Yii::import('ext.TanggalIndonesia');
		$id = Yii::app()->request->getParam('id');

		$dataKontent = WebKontent::model()->findByPk($id);

		$this->render('detail_event', array(
				'dataKontent' =>$dataKontent
			)
		);
	}

	public function actionDetail()
	{
		Yii::import('ext.TanggalIndonesia');
		$id            = Yii::app()->request->getParam('id');

		$model_pengaduan = Yii::app()->db->createCommand()
			->select('a.tblpengaduan_id,a.tblpengaduan_file,a.tblpengaduan_tanggal,a.tblpengaduan_keterangan,b.tblinventaris_namabarang')
			->from('tblpengaduan a')
			->join('tblinventaris b', 'b.tblinventaris_id=a.tblinventaris_id')
			->where('tblpengaduan_id=:id', array(':id'=>$id))
			->queryRow();

		$model_tanggapan_petugas = Tblpengaduan::model()->findByPk($id);

		$model_timeline = TimelinePengaduan::model()->findAll('tblpengaduan_id=:idpengaduan', array(':idpengaduan'=>$id));
		$this->render('detail', array(
				'model_pengaduan' =>$model_pengaduan,
				'model_timeline' =>$model_timeline,
				'model_tanggapan_petugas' =>$model_tanggapan_petugas
			)
		);
	}

	public function actionAbout()
	{
		$dataKontent = WebKontent::model()->find('tblwebmenu_id=:id', array(':id'=>2));

		$this->render('about', array(
				'dataKontent' =>$dataKontent
			)
		);
	}

	public function actionGaleri()
	{
		$dataGallery = Yii::app()->db->createCommand()
			->select('*')
			->from('tblgaleri')
			->where('tblgaleri_status="T"')
			->queryAll();

		$this->render('gallery', array(
				'dataGaleri' =>$dataGallery
			)
		);
	}

	public function actionKirimSaran()
	{
		$folder = "upload/pengaduan";
		$file = $_FILES['upload_file']['tmp_name']; 
		$namafileimage = md5(microtime()).'_'.$_FILES['upload_file']['name'];
		$target = dirname(Yii::app()->basePath) . '/'. $folder . '/' . $namafileimage;
		
		$allowed = array('jpg','jpeg','png','gif','bmp');
		$pecah = explode('.', $_FILES['upload_file']['name']);
		$getext = array_reverse($pecah);
		$ext = $getext[0];

		if(isset($_FILES["upload_file"]))
		{
			//Filter the file types , if you want.
			if (in_array(strtolower($ext), $allowed)) {

				if ($_FILES["upload_file"]["error"] > 0)
				{
				  echo "error ";
				}
				else
				{
					//move the uploaded file to uploads folder;
			    	//move_uploaded_file($file,$target);


					if (move_uploaded_file($file,$target)) {
						// echo $namafileimage;
						chmod($target, 0777);
					}
					else {
						echo "failed";
					}
			    	
				}
			}else{
				echo "invalid_ext";
			}	

		}

		$model = new Tblpengaduan;
		$model->tblinventaris_id = trim($_POST['id_inventaris']);
		$model->tblpengaduan_namapelapor = trim($_POST['namapelapor']);
		$model->tblpengaduan_keterangan = trim($_POST['keteranganlapor']);
		$model->tblpengaduan_tanggal = date('Y-m-d h:i:s');
		$model->tblpengaduan_file = $namafileimage;
		if($model->save()) {
			$timeline = new TimelinePengaduan;
			$timeline->tblpengaduan_id = $model->getPrimaryKey();
			$timeline->tblinventaris_id = trim($_POST['id_inventaris']);
			$timeline->tblpengaduan_namapelapor = trim($_POST['namapelapor']);
			$timeline->tblpengaduan_status = "DILAPORKAN";
			$timeline->tbltimelinepengaduan_datecreated = date('Y-m-d h:i:s');
			if($timeline->save()) {
				$GetEmailAdmin = Pengguna::model()->findAll('tblrole_id=1');

				$DtInventaris = Tblinventaris::model()->findByPk($model->tblinventaris_id);
				$from = 'ewsupy@gmail.com';
				$subject = 'EWS Inventaris - UPY';
				Yii::import('ext.TanggalIndonesia');
				$message = 'EWS Inventarisasi
				<br>Laporan Pengaduan Baru
				<br>No Barang : '.$DtInventaris->tblinventaris_nomor.'
				<br>Nama Barang : '.$DtInventaris->tblinventaris_namabarang.'
				<br>Keterangan Aduan : '.$model->tblpengaduan_keterangan.'
				<br>Tanggal Aduan : '.date('d',strtotime($model->tblpengaduan_tanggal)) . ' ' . TanggalIndonesia::getBulan(date('n',strtotime($model->tblpengaduan_tanggal))) . ' ' . date('Y',strtotime($model->tblpengaduan_tanggal)).'
				<br>
				<br>Mohon Segera Tindakan Lanjutin Aduan Ini!';
				// $to = 'cahyo.diginetmedia@gmail.com';

				$mail=Yii::app()->Smtpmail;
				foreach ($GetEmailAdmin as $key){
					$mail->AddAddress(trim($key->tblpengguna_email), "");
				}
				$mail->SetFrom($from, 'EWS Inventaris');
				$mail->Subject = $subject;
				$mail->MsgHTML($message);
				// $mail->AddAddress($to, "");
				if($mail->Send()) {
					echo "success";
					// echo "Message sent!";
				}else {
					echo "Mailer Error: " . $mail->ErrorInfo;
				}
				
			} else {
				echo "failed";
			}
		} else {
			echo "failed";
		}
	}
}
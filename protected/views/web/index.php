<section class="ftco-about ftco-counter img" id="home-section">
	<video autoplay muted loop id="myVideo">
		<source src="<?php echo Yii::app()->getBaseUrl(1) ?>/themes/web/video/opening.mp4" type="video/mp4">
		Your browser does not support HTML5 video.
	</video>
</section>

<section class="bg-light" id="news-section" style="padding-top: 15px;">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-md-7 heading-section text-center ftco-animate">
				<h2 class="mb-4" style="color: white;">Berita</h2>
			</div>
		</div>
		<div class="row d-flex">
			<?php if(count($data['news']) > 0): ?>
			<?php foreach($data['news'] as $news): ?>
			<div class="col-md-3 d-flex ftco-animate">
				<div class="blog-entry justify-content-end">
					<a href="<?php echo Yii::app()->getBaseUrl(1) . "/web/news/" . $news['tblwebkontent_id'] ?>" class="block-20 content-image-news" style="background-image: url('<?php echo Yii::app()->getBaseUrl(1) ?>/upload/kontent/<?php echo $news['tblwebkontent_file'] ?>');background-size: 100%;">
					</a>
					<div class="text float-center content-news">
						<h3 class="heading"><a href="<?php echo Yii::app()->getBaseUrl(1) . "/web/news/" . $news['tblwebkontent_id'] ?>"><?php echo $news['tblwebkontent_judul'] ?></a></h3>
					</div>
				</div>
			</div>
			<?php endforeach ?>
			<?php else: ?>
			<div class="col-md-12 d-flex ftco-animate">
				<div class="blog-entry justify-content-end">
					<div class="text-center">
						<h5 class="heading" style="color:wheat;"><i> - Belum ada pembaharuan data "Berita" - </i></h5>
					</div>
				</div>
			<?php endif ?>
		</div>
	</div>
</section>

<section class="contact-section ftco-no-pb" id="event-section" style="padding-top: 15px;padding-bottom: 15px !important;background-color: #EDECD8;">
	<div class="row">
		<div class="col-md-6 heading-section ftco-animate">
			<h2 class="text-center">Event</h2>
			<div class="sidebar-box ftco-animate section-event">
				<div class="container">
					<ul class="event">
						<?php if(count($data['event']) > 0): ?>
						<?php foreach($data['event'] as $event): ?>
							<li>
								<a href="<?php echo Yii::app()->getBaseUrl(1) . "/web/event/" . $event['tblwebkontent_id'] ?>">
									<h5 class="heading"><b><?php echo $event['tblwebkontent_judul'] ?></b></h5>
									<?php echo $event['tblwebkontent_isi']; ?>
								</a>
							</li>
						<?php endforeach ?>
						<?php else: ?>
							<li>
								<div class="text-center">
									<h5 class="heading"><i> - Belum ada pembaharuan data "EVENT" - </i></h5>
								</div>
							</li>
						<?php endif ?>
					</ul>
				</div>
			</div>
		</div>
		<div class="col-md-3 heading-section ftco-animate" style="border-left: 1px solid #333;">
			<h3 class="text-center"><b>Sosial Media</b></h3>
			<hr>
			<div class="container text-center">
				<span><b>Youtube</b></span>
				<iframe class="latestVideoEmbed" vnum='0' cid="UCm529B0TWYxGKPuRn35tYJg" width="100%" height="250" frameborder="0" allowfullscreen></iframe>
				<p id="video-title">Video Title</p>
			</div>
			<hr>
			<div class="container text-center">
				<span><b>Instagram</b></span>
				<div width="100%" height="250" id="instafeed-container"></div>
			</div>
		</div>
		<div class="col-md-3 heading-section ftco-animate" style="border-left: 1px solid #333;">
			<?php if (isset($_SERVER['HTTPS']) && ($_SERVER['HTTPS'] == 'on' || $_SERVER['HTTPS'] == 1) || isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https') {
				$protocol = 'https://';
			} else {
				$protocol = 'http://';
			} ?>
			<h3 class="text-center"><b>Kalender Liturgi</b></h3>
			<div class="container">
				<iframe align="center"  frameborder="1" width="100%" height="250px" scrolling="No" src="<?php echo $protocol; ?>www.imankatolik.or.id/kalender_share_kecil.php"></iframe>
			</div>
		</div>
	</div>
</section>
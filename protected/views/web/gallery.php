<section class="ftco-section" style="padding-top: 150px;">
    <div class="overlay"></div>
    <div class="container">
        <div class="row no-gutters slider-text align-items-end justify-content-center">
            <div class="col-md-9 ftco-animate pb-5 text-center">
                <h1 class="mb-3 bread">Galeri</h1>
            </div>
        </div>
    </div>
</section>
		

<section class="ftco-section">
    <div class="container">
        <div class="row">
            <div class="ftco-animate" style="text-align: center;">
            <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
                <body>
                    <div>
                        <?php foreach($dataGaleri as $data): ?>
                        <div class="folder-grid" onclick="openURL('<?php echo $data['tblgaleri_link'] ?>')">
                            <span>
                                <i class="fa fa fa-folder fa-fw" style="color:#a3a3a3;"></i>
                            <?php echo $data['tblgaleri_judul'] ?>
                            </span>
                        </div>
                        <?php endforeach; ?>
                        <!-- <div class="folder-grid">
                            </span>
                                <i class="fa fa fa-folder fa-fw" style="color:#a3a3a3;"></i>
                            Folder 1
                            </span>
                        </div>
                        <div class="folder-grid">
                            </span>
                                <i class="fa fa fa-folder fa-fw" style="color:#a3a3a3;"></i>
                            Folder 2
                            </span>
                        </div> -->
                    </div>
                    <!-- <br>
                    <div>
                        <div class="file-grid">
                            <div class="file-thumnail">
                                <i class="fa fa fa-file fa-fw" style="color:#a3a3a3;"></i>
                            </div>
                            <div class="file-title">Lingkungan Santo Gabriel</div>
                        </div>
                        <div class="file-grid">
                            <div class="file-thumnail">
                                <i class="fa fa fa-file fa-fw" style="color:#a3a3a3;"></i>
                            </div>
                            <div class="file-title">IC3.pdf</div>
                        </div>
                    </div> -->
                </body>
            </div>
        </div>
    </div>
</section> <!-- .section -->

<script type="text/javascript">
    function openURL(url) {
        window.open(url, '_blank').focus();
    }
</script>

<style type="text/css">
    body {
    font-family: 'Open Sans', sans-serif;
    background: #E8E8E8;
    }
    .folder-grid {
    display: inline-block;
    color: #424242;
    margin: 5px;
    /* width: 185px; */
    padding: 13px;
    background: #fff;
    cursor: pointer;
    transition-duration: 0.20s;
    overflow: hidden;
    text-overflow: ellipsis;
    white-space: nowrap;
    box-shadow: 0 2px 2px rgba(0,0,0,.15);
    }

    .folder-grid:hover {
    background: #dbdbdb;
    box-shadow: 0 2px 2px rgba(0,0,0,.2);
    }
    .file-grid {
    display: inline-block;
    box-shadow: 0 2px 2px rgba(0,0,0,.15);
    width: 210px;
    color: #424242;
    margin: 5px;
    transition-duration: 0.20s;
    background: #fff;
    cursor: pointer;
    }

    .file-thumnail {
    text-align: center;
    padding: 40px;
    font-size: -webkit-xxx-large;
    background-color: white;
    }

    .file-title {
    width: 100%;
    padding: 13px;
    overflow: hidden;
    text-overflow: ellipsis;
    white-space: nowrap;
    }

    .file-grid:hover {
    background: #dbdbdb;
    box-shadow: 0 2px 2px rgba(0,0,0,.2);
    }
</style>
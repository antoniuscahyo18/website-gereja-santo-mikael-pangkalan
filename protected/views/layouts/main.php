<!DOCTYPE html>
<html lang="en">
<head>
	<title><?php echo CHtml::encode($this->pageTitle); ?></title>
	<link rel="shortcut icon" href="<?php echo Yii::app()->request->baseUrl; ?>/images/favicon.png" type="image/x-icon">
		<link rel="icon" href="<?php echo Yii::app()->request->baseUrl; ?>/images/favicon.png" type="image/x-icon">
		
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	
	<link href="https://fonts.googleapis.com/css?family=Poppins:100,200,300,400,500,600,700,800,900" rel="stylesheet">
	
	<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/themes/web/css/open-iconic-bootstrap.min.css">
	<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/themes/web/css/animate.css">
	
	<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/themes/web/css/owl.carousel.min.css">
	<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/themes/web/css/owl.theme.default.min.css">
	<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/themes/web/css/magnific-popup.css">
	
	<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/themes/web/css/aos.css">
	
	<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/themes/web/css/ionicons.min.css">
	
	<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/themes/web/css/flaticon.css">
	<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/themes/web/css/icomoon.css">
	<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/themes/web/css/style.css">
</head>

<body data-spy="scroll" data-target=".site-navbar-target" data-offset="300">
	<nav class="navbar navbar-expand-lg navbar-light ftco_navbar bg-light ftco-navbar-light site-navbar-target" id="ftco-navbar">
		<div class="container">
			<a class="navbar-brand" href="single.html" width="50%">
				<img src="<?php echo Yii::app()->getBaseUrl(1) ?>/themes/web/images/logo_pangkalan.png" height="60px">
			</a>
			<button class="navbar-toggler js-fh5co-nav-toggle fh5co-nav-toggle" type="button" data-toggle="collapse" data-target="#ftco-nav" aria-controls="ftco-nav" aria-expanded="false" aria-label="Toggle navigation">
				<span class="oi oi-menu"></span> Menu
			</button>
			
			<div class="collapse navbar-collapse" id="ftco-nav">
				<ul class="navbar-nav nav ml-auto">
					<li class="nav-item"><a href="#home-section" class="nav-link"><span>Home</span></a></li>
					<li class="nav-item"><a href="#about-section" class="nav-link"><span>About</span></a></li>
					<li class="nav-item"><a href="#news-section" class="nav-link"><span>News</span></a></li>
					<li class="nav-item"><a href="#event-section" class="nav-link"><span>Event</span></a></li>
					<li class="nav-item"><a href="#service-section" class="nav-link"><span>Service</span></a></li>
					<li class="nav-item"><a href="#galerry-section" class="nav-link"><span>Gallery</span></a></li>
				</ul>
			</div>
		</div>
	</nav>

    <?php echo $content; ?>

    <footer class="ftco-footer">
		<div class="container">
			<div class="row">
				<div class="col-md" style="padding-top: 50px;">
					<div class="ftco-footer-widget mb-8">
						<img src="<?php echo Yii::app()->getBaseUrl(1) ?>/themes/web/images/logo_pangkalan.png" height="50px">
						<div class="block-23 mb-3" style="padding-top: 20px;">
							<ul>
								<li><span class="icon icon-map-marker"></span><span class="text">Pangkalan TNI AU, Jl. Lettu TPT Sapardal Karang Jambe, Kec. Banguntapan Kabupaten Bantul, Daerah Istimewa Yogyakarta 55281</span></li>
								<li><a href="#"><span class="icon icon-phone"></span><span class="text">+2 392 3929 210</span></a></li>
								<li><a href="#"><span class="icon icon-envelope"></span><span class="text">parokipangkalan@gmail.com</span></a></li>
							</ul>
						</div>
						<ul class="ftco-footer-social list-unstyled float-md-left float-lft mt-5">
							<li class="ftco-animate"><a href="#"><span class="icon-twitter"></span></a></li>
							<li class="ftco-animate"><a href="#"><span class="icon-facebook"></span></a></li>
							<li class="ftco-animate"><a href="#"><span class="icon-instagram"></span></a></li>
						</ul>
					</div>
				</div>
				<div class="col-md" style="padding-top: 50px;">
					<div class="ftco-footer-widget mb-4">
						<h2 class="ftco-heading-2">Lingkungan</h2>
						<ul class="list-unstyled">
							<li><a href="#"><span class="icon-radio_button_unchecked mr-2"></span>St. Gabriel Maredan</a></li>
							<li><a href="#"><span class="icon-radio_button_unchecked mr-2"></span>St. Pius X Pelem</a></li>
							<li><a href="#"><span class="icon-radio_button_unchecked mr-2"></span>St. Ignatius Loyola Karangbendo</a></li>
							<li><a href="#"><span class="icon-radio_button_unchecked mr-2"></span>St. Fransiskus Xaverius Karangjambe</a></li>
							<li><a href="#"><span class="icon-radio_button_unchecked mr-2"></span>St. Rafael Pangkalan</a></li>
							<li><a href="#"><span class="icon-radio_button_unchecked mr-2"></span>St. Petrus Faber Gatak</a></li>
							<li><a href="#"><span class="icon-radio_button_unchecked mr-2"></span>St. Andreas Rasul Wonocatur Timur</a></li>
							<li><a href="#"><span class="icon-radio_button_unchecked mr-2"></span>Mgr. Soegijapranata Wonocatur Barat</a></li>
						</ul>
					</div>
				</div>
				<div class="col-md" style="padding-top: 50px;color:white;">
					<div class="ftco-footer-widget mb-4">
						<h2 class="ftco-heading-2">Statistik Pengunjung</h2>
						<ul class="list-unstyled">
							<li>
								<div class="pengunjung-online">
									<span class="text">Pengunjung Online : </span>
									<div class="pengunjung-online-detail">1</div>
								</div>
							</li>
							<li><span class="text">Hari Ini : </span>2</li>
							<li><span class="text">Kemarin : </span>1</li>
							<li><span class="text">Total : </span>3</li>
						</ul>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12 text-center">
					<p>
						Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | Gereja Katolik Paroki Santo Mikael Pangkalan</a>
					</p>
				</div>
			</div>
		</div>
	</footer>
	
	<!-- loader -->
	<div id="ftco-loader" class="show fullscreen"><svg class="circular" width="48px" height="48px"><circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee"/><circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10" stroke="#F96D00"/></svg></div>
	
	<script src="<?php echo Yii::app()->getBaseUrl(1) ?>/themes/web/js/jquery.min.js"></script>
	<script src="<?php echo Yii::app()->getBaseUrl(1) ?>/themes/web/js/jquery-migrate-3.0.1.min.js"></script>
	<script src="<?php echo Yii::app()->getBaseUrl(1) ?>/themes/web/js/popper.min.js"></script>
	<script src="<?php echo Yii::app()->getBaseUrl(1) ?>/themes/web/js/bootstrap.min.js"></script>
	<script src="<?php echo Yii::app()->getBaseUrl(1) ?>/themes/web/js/jquery.easing.1.3.js"></script>
	<script src="<?php echo Yii::app()->getBaseUrl(1) ?>/themes/web/js/jquery.waypoints.min.js"></script>
	<script src="<?php echo Yii::app()->getBaseUrl(1) ?>/themes/web/js/jquery.stellar.min.js"></script>
	<script src="<?php echo Yii::app()->getBaseUrl(1) ?>/themes/web/js/owl.carousel.min.js"></script>
	<script src="<?php echo Yii::app()->getBaseUrl(1) ?>/themes/web/js/jquery.magnific-popup.min.js"></script>
	<script src="<?php echo Yii::app()->getBaseUrl(1) ?>/themes/web/js/aos.js"></script>
	<script src="<?php echo Yii::app()->getBaseUrl(1) ?>/themes/web/js/jquery.animateNumber.min.js"></script>
	<script src="<?php echo Yii::app()->getBaseUrl(1) ?>/themes/web/js/scrollax.min.js"></script>
	<script src="<?php echo Yii::app()->getBaseUrl(1) ?>/themes/web/js/main.js"></script>
	<style>
		.carousel-caption p {
			/* margin-bottom: 20px; */
			font-size: 21px;
			line-height: 1.4;
		}

		.section-event {
			text-align: justify;
		}

		.pengunjung-online {
			display: inline-block;
			background: #1cb71c;
			padding-top: 8px;
			padding-bottom: 8px;
			padding-left: 10px;
			padding-right: 10px;
			border-radius: 6px;
		}
		.pengunjung-online-detail {
			display: inline-block;
			background: #cb1919;
			padding-left: 10px;
			padding-right: 10px;
			border-radius: 6px;
		}
	</style>
</body>
</html>

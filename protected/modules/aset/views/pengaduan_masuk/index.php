<?php define('ASSETS_URL', 'themes/smartadmin'); ?>
<div class="row">
	<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		<div class="jarviswidget jarviswidget-color-red" id="wid-id-data_aduanmasuk" data-widget-editbutton="false" data-widget-deletebutton="false" data-widget-fullscreenbutton="false">
			<header style="border-radius: 5px 5px 0px 0px;">
				<span class="widget-icon"> <i class="fa fa-table"></i> </span>
				<h2>Data Aduan</h2>
			</header>
			<div>
				<div class="jarviswidget-editbox">
				</div>
				<div class="widget-body no-padding">
					<div class="" style="width: auto;overflow-x: auto">
						<table id="dt_basic" class="table table-bordered table-striped table-condensed table-hover smart-form has-tickbox">
							<thead>
								<tr>
									<th width="30">No</th>
									<th>Nomor Inventaris</th>
									<th>Nama Barang</th>
									<th>Detail Barang</th>
									<th>Nama Pelapor</th>
									<th>Tanggal Aduan</th>
									<th>Keterangan Aduan</th>
									<th>Gambar Pendukung</th>
									<th>Status</th>
								</tr>
							</thead>
							<tbody>
								<?php $no=1;foreach ($data['pengaduan'] as $list):?>
									<tr>
										<td><?php echo $no++ ?></td>
										<td>
											<strong><?php echo $list['tblinventaris_nomor'] ?></strong>
										</td>
										<td><?php echo $list['tblinventaris_namabarang'] ?> <br> [Aset <?php echo $list['refjenisaset_nama'] ?>]</td>
										<td align="center">
											<button onclick="detailbarang('<?php echo $list['tblinventaris_id'] ?>')" class="btn btn-sm btn-success" data-toggle="tooltip" data-placement="bottom" title="Detail Barang"><i class="fa fa-eye"> Detail Barang</i> </button>
										</td>
										<td><?php echo $list['tblpengaduan_namapelapor'] ?></td>
										<td><?php echo date('d',strtotime($list['tblpengaduan_tanggal'])) . ' ' . TanggalIndonesia::getBulan(date('m',$list['tblpengaduan_tanggal'])) . ' ' . date('Y',strtotime($list['tblpengaduan_tanggal'])) ?></td>
										<td><?php echo $list['tblpengaduan_keterangan'] ?></td>
										<td>
											<center>
												<a class="btn btn-xs bg-color-green txt-color-white"  target="_blank" href="<?php echo Yii::app()->getBaseUrl(1) ?>/upload/pengaduan/<?php echo $list['tblpengaduan_file'] ?>">Lihat File</a>
											</center>
										</td>	
										<td>
											<?php if($list['tblpengaduan_status']==NULL): ?>
												<button onclick="diterima('<?php echo $list['tblpengaduan_id'] ?>')" class="btn btn-sm btn-primary" data-toggle="tooltip" data-placement="bottom" title="Klik Untuk Menerima Pengaduan"><i class="fa fa-edit"> Diterima </i> </button>
											<?php elseif($list['tblpengaduan_status']=='DITERIMA'): ?>
												<div id="status">
													<label style="font-weight: 800;color: #3276b1;">DI TERIMA</label>
												</div>
												<button onclick="proses('<?php echo $list['tblpengaduan_id'] ?>')" class="btn btn-sm btn-warning" data-toggle="tooltip" data-placement="bottom" title="Proses"><i class="fa fa-edit"> Proses</i> </button>
											<?php elseif($list['tblpengaduan_status']=='PROSES'): ?>
												<div id="status">
													<label style="font-weight: 800;color: #b98822;">DI PROSES</label>
												</div>
												<button onclick="selesai('<?php echo $list['tblpengaduan_id'] ?>')" class="btn btn-sm btn-success" data-toggle="tooltip" data-placement="bottom" title="Selesai"><i class="fa fa-check"> Selesai</i> </button>
											<?php elseif($list['tblpengaduan_status']=='SELESAI'): ?>
												<div id="status">
													<label style="font-weight: 800;color: #197d19;">SELESAI</label>
												</div>
											<?php endif; ?>
										</td>
									</tr>
								<?php endforeach; ?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</article>
</div>


<!-- Modal detail barang -->
<div class="modal fade" id="modal_detailbarang" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-keyboard="false" data-backdrop="static">
	<div class="modal-dialog modal-md">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
					&times;
				</button>
				<h4 class="modal-title">
					Detail Barang
				</h4>
			</div>
			<div class="modal-body no-padding">

				<form id="form-data" class="smart-form">
					<fieldset style="padding-top: 15px !important;">

						<div class="row">
							<section class="col col-3">
								<label > 
									<strong>Spesifikasi	: </strong>						
								</label>
							</section>
							<section class="col col-9">
								<label id="spesifikasi"> 
								</label>
							</section>
						</div>

						<div class="row">
							<section class="col col-3">
								<label > 
									<strong>Tahun Perolehan : </strong>								
								</label>
							</section>
							<section class="col col-9">
								<label id="tahun_perolehan"> 
								</label>
							</section>									
						</div>

						<div class="row">
							<section class="col col-3">
								<label > 
									<strong>Ruang : </strong>										
								</label>
							</section>
							<section class="col col-9">
								<label id="ruang"> 
								</label>
							</section>									
						</div>

						<div class="row">
							<section class="col col-3">
								<label > 
									<strong>File : </strong>										
								</label>
							</section>
							<section class="col col-9">
								<img style="width: 100%" id="file" src="">
							</section>									
						</div>


					</fieldset>
					<footer>
						<button type="reset" class="btn btn-default" data-dismiss="modal">
							Tutup
						</button>
					</footer>
				</form>
			</div>

		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div>
<!-- Modal detail barang -->

<!-- Modal proses -->
<div class="modal fade" id="modal_proses" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-keyboard="false" data-backdrop="static">
	<div class="modal-dialog modal-md">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
					&times;
				</button>
				<h4 class="modal-title">
					Form Proses
				</h4>
			</div>
			<div class="modal-body no-padding">

				<form id="form-data" class="smart-form">
					<fieldset style="padding-top: 15px !important;">

						<div class="row">
							<section class="col col-3">
								<label > 
									Keterangan Tanggapan Proses								
								</label>
							</section>
							<section class="col col-9">
								<label class="input">
									<textarea class="form-control" placeholder="" rows="4" id="ket_tanggapan" name="ket_tanggapan"></textarea>
								</label>
							</section>
						</div>

						<!-- <div id="formuploadnya" style="display: none;">
							<div class="row">
								<section class="col col-3">
									<label > 
										Upload File										
									</label>
								</section>
								<section class="col col-9">
									<label for="file" class="input input-file">
										<div class="button">
											<input type="file" id="upload_file" name="upload_file" onchange="this.parentNode.nextSibling.value = this.value">Browse</div><input id="namafileimages" type="text" >
											<input type="hidden" name="namafileimage" id="namafileimage" value="">
										</label>
									</section>									
								</div>

								<div class="row">
									<section class="col col-3">
										<label > 
											&nbsp;										
										</label>
									</section>
									<section class="col col-9">
										<div class="progress progress-md progress-striped active" id="progress">
											<div class="progress-bar bg-color-blue"  id="bar"  role="progressbar" style="width: auto;"></div>
										</div>
									</section>									
								</div>
							</div> -->

					</fieldset>
					<footer>
						<a onclick="SimpanProses()" id="btnSewaAset" type="button" data-dismiss="modal" class="btn btn-primary">
							Simpan
						</a>
						<button type="reset" class="btn btn-default" data-dismiss="modal">
							Batal
						</button>
					</footer>
				</form>
			</div>

		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div>
<!-- Modal proses -->

<!-- Modal Selesai -->
<div class="modal fade" id="modal_selesai" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-keyboard="false" data-backdrop="static">
	<div class="modal-dialog modal-md">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
					&times;
				</button>
				<h4 class="modal-title">
					Form Selesai
				</h4>
			</div>
			<div class="modal-body no-padding">

				<form id="form-data" class="smart-form">
					<fieldset style="padding-top: 15px !important;">

						<div class="row">
							<section class="col col-3">
								<label > 
									Keterangan Tanggapan Selesai								
								</label>
							</section>
							<section class="col col-9">
								<label class="input">
									<textarea class="form-control" placeholder="" rows="4" id="ket_tanggapan_selesai" name="ket_tanggapan_selesai"></textarea>
								</label>
							</section>
						</div>

					</fieldset>
				</form>
						
				<form method="post" enctype="multipart/form-data" class="smart-form" action="aset/pengaduan_masuk/SimpanFile" id="form-upload-temp">
					<fieldset>
						<div id="formuploadnya">
							<div class="row">
								<section class="col col-3">
									<label > 
										Upload File										
									</label>
								</section>
								<section class="col col-9">
									<label for="file" class="input input-file">
										<div class="button">
										<input type="file" id="upload_file" name="upload_file" onchange="this.parentNode.nextSibling.value = this.value" accept="image/png, image/jpeg, image/jpg">Browse</div><input id="namafileimages" type="text">
										<input type="hidden" name="namafileimage" id="namafileimage" value="">
									</label>
								</section>									
							</div>

							<div class="row">
								<section class="col col-3">
									<label > 
										&nbsp;										
									</label>
								</section>
								<section class="col col-9">
									<div class="progress progress-md progress-striped active" id="progress">
										<div class="progress-bar bg-color-blue"  id="bar"  role="progressbar" style="width: auto;"></div>
									</div>
								</section>									
							</div>

							<div class="row">
								<section class="col col-3">
									<label > 
										&nbsp;										
									</label>
								</section>
								<section class="col col-9">
									<button type="submit" id="submit-file" style="display: none;" class="btn btn-sm btn-block bg-color-blue btn-success">
										<i class="fa fa-upload"></i> Upload File
									</button>
								</section>									
							</div>
						</div>
					</fieldset>
					<footer>
						<a onclick="simpan()" id="btnSewaAset" type="button" data-dismiss="modal" class="btn btn-primary">
							Simpan
						</a>
						<button type="reset" class="btn btn-default" data-dismiss="modal">
							Batal
						</button>
					</footer>
				</form>
			</div>

		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div>
<!-- Modal Selesai -->

<script type="text/javascript">	
	pageSetUp();

	jQuery(document).ready(function($) {
		reloadDT('dt_basic');
	});

	function detailbarang(id) {
		$('#modal_detailbarang').modal('show');
		$.ajax({
			url: 'aset/pengaduan_masuk/GetDetailBarang',
			type: 'POST',
			dataType:'json',
			data: {id: id},
			success : function (respon) {
				$("#spesifikasi").text(respon.spesifikasi);
				$("#tahun_perolehan").text(respon.tahun_perolehan);
				$("#ruang").text(respon.ruang);
				$("#file").attr('src','<?php echo Yii::app()->baseUrl; ?>/upload/aset/'+respon.file);
			}

		});
	}

	function diterima(id) {
		window.id = id;
		window.cmd = "diterima";
		$.SmartMessageBox({
			title : "Konfirmasi",
			content : "Apakah anda yakin akan Menerima Pengaduan ini?",
			buttons : '[Tidak][Ya]'
		}, function(ButtonPressed) {
			if (ButtonPressed === "Ya") {
				$.ajax({
					url: 'aset/pengaduan_masuk/terima',
					type: 'POST',
					data: {id: id},
					success: function  (respon) {
						if (respon=='success') {
							notifikasi("Sukses","Data berhasil diterima","success");
						} else {
							notifikasi("Gagal","Data gagal diterima","failed",1,0);
						}
					}
				});

			}
		});
	}

	function proses(id) {
		$('#modal_proses').modal('show');
		window.idproses = id;
	}

	function SimpanProses() {
		$.ajax({
			url: 'aset/pengaduan_masuk/SimpanProses',
			type: 'POST',
			data: {
				id: window.idproses,
				keterangan_tanggapan: $('#ket_tanggapan').val()
			},
			success: function  (respon) {
				if (respon=='success') {
					notifikasi("Sukses","Data berhasil diterima","success");
				} else {
					notifikasi("Gagal","Data gagal diterima","failed",1,0);
				}
			}
		});
	}

	function selesai(id) {
		window.idselesai = id;
		$('#modal_selesai').modal('show');
	}

	loadScript("<?php echo ASSETS_URL; ?>/js/jquery.form.js",defineAjaxForm2);
	function defineAjaxForm2() {
		var allowed = ['jpg','jpeg','png','gif','bmp'];
		var options = { 
			beforeSend: function() 
			{
				$("#progress").show();
		    	//clear everything
		    	$("#bar").width('0%');

		    	filenya = document.getElementById("upload_file");
		    	ext = $("#upload_file").val().split('.').reverse()[0];
		    	if (!inArray(ext,allowed) && ext!='') {
		    		console.log(ext);
		    		$("#upload_file").parent().next().val('');
		    		notifikasi('Peringatan','File yang anda pilih tidak sesuai extensi yang diizinkan! Mohon pilih file berekstensi .jpg/jpeg, .png, .giv, dan .bmp ','false',1,0);
		    		xhr.abort();
		    		return false;
		    	}

		    },
		    uploadProgress: function(event, position, total, percentComplete) 
		    {
		    	$("#bar").width(percentComplete+'%');

		    },
		    success: function() 
		    {
		    	$("#bar").width('100%');

		    },
		    complete: function(response) 
		    {
		    	if (response.responseText=='invalid_ext') {
		    		notifikasi('Peringatan','File yang anda pilih tidak sesuai extensi yang diizinkan! Mohon pilih file berekstensi .jpg/jpeg, .png, .giv, dan .bmp ','false',1,0);
		    		return false;
		    	}else{
		    		window.file = response.responseText;
		    		SimpanSelesai();
		    	}
		    },
		    error: function()
		    {
		    	alert("error");

		    }

		}; 

		$("#form-upload-temp").ajaxForm(options);
	}

	function simpan() {
		if ($('#upload_file').val()=='') {
			SimpanSelesai()
		}else{
			$("#submit-file").click();
		};
	}
	

	function SimpanSelesai() {
			$.ajax({
				url: 'aset/pengaduan_masuk/Selesai',
				type: 'POST',
				data: {
					id: window.idselesai,
					ket_tanggapan_selesai : $("#ket_tanggapan_selesai").val(),
					file : window.file,
				}
				,success: function  (respon) {
				//$("#submit-file").click();
				
				if (respon=='success'){
					notifikasi("Sukses","Data berhasil disimpan","success");
				} 
				else{
					notifikasi("Gagal","Data gagal disimpan","failed");
				}
			}
		})
		return false;
	}

</script>

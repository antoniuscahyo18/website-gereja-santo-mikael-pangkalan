<?php

class Pengaduan_masukController extends Controller
{
	public function actionIndex()
	{
		Yii::import('ext.TanggalIndonesia');
		
		$data['pengaduan'] = Yii::app()->db->createCommand()
		->select('a.tblpengaduan_id,
			a.tblpengaduan_file,
			a.tblpengaduan_tanggal,
			a.tblpengaduan_keterangan,
			a.tblpengaduan_namapelapor,
			a.tblpengaduan_status,
			b.tblinventaris_id,
			b.tblinventaris_namabarang,
			b.tblinventaris_nomor,
			c.refjenisaset_nama')
		->from('tblpengaduan a')
		->join('tblinventaris b', 'b.tblinventaris_id=a.tblinventaris_id')
		->join('refjenisaset c', 'b.refjenisaset_id=b.refjenisaset_id')
		->queryAll();
		$this->renderPartial('index', array('data' => $data));
	}

	public function actionGetDetailBarang()
	{
		$id = Yii::app()->request->getParam('id');
		$DataInventaris = Tblinventaris::model()->findByPk($id);
		$ArrData = [
			'nama_barang' => $DataInventaris->tblinventaris_namabarang,
			'spesifikasi' => $DataInventaris->tblinventaris_spesifikasi,
			'tahun_perolehan' => Reftahun::model()->findByPk($DataInventaris->reftahun_id)->reftahun_nama,
			'ruang' => Refruang::model()->findByPk($DataInventaris->refruang_id)->refruang_nama,
			'file' => $DataInventaris->tblinventaris_file,
		];
		// print_r($DataInventaris);die();
		echo CJSON::encode($ArrData);
	}

	public function actionTerima()
	{
		$id = Yii::app()->request->getParam('id');
		$model = Tblpengaduan::model()->findByPk($id);
		$model->tblpengaduan_status = 'DITERIMA';
		if($model->save()) {
			$timeline = new TimelinePengaduan;
			$timeline->tblpengaduan_id = $id;
			$timeline->tblinventaris_id = $model->tblinventaris_id;
			$timeline->tblpengaduan_namapelapor = $model->tblpengaduan_namapelapor;
			$timeline->tblpengaduan_status = "DITERIMA";
			$timeline->tbltimelinepengaduan_datecreated = date('Y-m-d h:i:s');
			if($timeline->save()) {
				echo "success";
			} else {
				echo "failed";
			}
		} else {
			echo "failed";
		}
	}

	public function actionSimpanProses()
	{
		$id = Yii::app()->request->getParam('id');
		$keterangan = Yii::app()->request->getParam('keterangan_tanggapan');
		$model = Tblpengaduan::model()->findByPk($id);
		$model->tblpengaduan_status = 'PROSES';
		$model->tblpengaduan_keterangan_tanggapan_proses = $keterangan;
		if($model->save()) {
			$timeline = new TimelinePengaduan;
			$timeline->tblpengaduan_id = $id;
			$timeline->tblinventaris_id = $model->tblinventaris_id;
			$timeline->tblpengaduan_namapelapor = $model->tblpengaduan_namapelapor;
			$timeline->tblpengaduan_status = "PROSES";
			$timeline->tbltimelinepengaduan_datecreated = date('Y-m-d h:i:s');
			if($timeline->save()) {
				echo "success";
			} else {
				echo "failed";
			}
		} else {
			echo "failed";
		}
	}

	public function actionSelesai()
	{
		$id = Yii::app()->request->getParam('id');
		$ket_tanggapan_selesai = Yii::app()->request->getParam('ket_tanggapan_selesai');
		$file = Yii::app()->request->getParam('file');
		// echo $id;die();

		$model = Tblpengaduan::model()->findByPk($id);
		$model->tblpengaduan_status = 'SELESAI';
		$model->tblpengaduan_keterangan_tanggapan_selesai = $ket_tanggapan_selesai;
		$model->tblpengaduan_file_tanggapan = $file;
		if($model->save()) {
			$timeline = new TimelinePengaduan;
			$timeline->tblpengaduan_id = $id;
			$timeline->tblinventaris_id = $model->tblinventaris_id;
			$timeline->tblpengaduan_namapelapor = $model->tblpengaduan_namapelapor;
			$timeline->tblpengaduan_status = "SELESAI";
			$timeline->tbltimelinepengaduan_datecreated = date('Y-m-d h:i:s');
			if($timeline->save()) {
				echo "success";
			} else {
				echo "failed";
			}
		} else {
			echo "failed";
		}
	}

	public function actionSimpanFile()
	{
		$folder = "upload/pengaduan_tanggapan";
		$filertf = $_FILES['upload_file']['tmp_name']; 
		$namafileimage = md5(microtime()).'_'.$_FILES['upload_file']['name'];
		$target = dirname(Yii::app()->basePath) . '/'. $folder . '/' . $namafileimage;
		
		$allowed = array('jpg','jpeg','png','gif','bmp');
		$pecah = explode('.', $_FILES['upload_file']['name']);
		$getext = array_reverse($pecah);
		$ext = $getext[0];

		if(isset($_FILES["upload_file"]))
		{
			//Filter the file types , if you want.
			if (in_array(strtolower($ext), $allowed)) {

				if ($_FILES["upload_file"]["error"] > 0)
				{
				  echo "error ";
				}
				else
				{
					//move the uploaded file to uploads folder;
			    	//move_uploaded_file($filertf,$target);


					if (move_uploaded_file($filertf,$target)) {
						echo $namafileimage;
						chmod($target, 0777);
					}
					else {
						echo "failed";
					}
			    	
				}
			}else{
				echo "invalid_ext";
			}	

		}
	}

	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}
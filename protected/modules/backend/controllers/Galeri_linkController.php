<?php

class Galeri_linkController extends Controller
{
	public function actionIndex()
	{
		$data['data_galeri'] = GaleriLink::model()->findAll();
		$this->renderPartial('index', array(
			'data'=>$data
		));
	}

	public function actionSimpan()
	{
		if(!Yii::app()->request->isPostRequest)
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
		$cmd = trim($_POST['cmd']); // tangkap perintah yg dikirim

		$tblgaleri_judul = trim($_POST['tblgaleri_judul']);
		$tblgaleri_link = trim($_POST['tblgaleri_link']);
		$tblgaleri_status = trim($_POST['tblgaleri_status']);

		if ($cmd=="tambah") {
			$model = new GaleriLink;
			$model->tblgaleri_created = date('Y-m-d H:i:s');
		}
		elseif($cmd=="edit") {
			$id = trim($_POST['id']); 
			$model = GaleriLink::model()->findByPk($id);
			$model->tblgaleri_updated = date('Y-m-d H:i:s');
		}
		else{
			echo "Invalid Command!";
			Yii::app()->end();
		}
		
		$model->tblgaleri_judul = $tblgaleri_judul;
		$model->tblgaleri_link = $tblgaleri_link;
		$model->tblgaleri_status = $tblgaleri_status;


		if($model->save()){	
			echo "success";
		} 
		else{
			print_r($model);
			echo "failed";
		}
	}

	public function actionGetData()
	{
		if(!Yii::app()->request->isPostRequest)
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
		$id = trim($_POST['id']);
		$model = GaleriLink::model()->findByPk($id);
		echo CJSON::encode($model);
	}

	public function actionHapusData()
	{
		if(!Yii::app()->request->isPostRequest)
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
		$id = trim($_POST['id']);
		$model = GaleriLink::model()->findByPk($id);
		if ($model->delete()) {
			echo "success";
		}
		else {
			echo "failed";
		}
	}

	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}

<?php

/**
 * This is the model class for table "tblpengaduan".
 *
 * The followings are the available columns in table 'tblpengaduan':
 * @property integer $tblpengaduan_id
 * @property integer $tblinventaris_id
 * @property string $tblpengaduan_namapelapor
 * @property string $tblpengaduan_keterangan
 * @property string $tblpengaduan_keterangan_tanggapan_proses
 * @property string $tblpengaduan_keterangan_tanggapan_selesai
 * @property string $tblpengaduan_file
 * @property string $tblpengaduan_file_tanggapan
 * @property string $tblpengaduan_tanggal
 * @property string $tblpengaduan_status
 */
class Tblpengaduan extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tblpengaduan';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('tblinventaris_id', 'required'),
			array('tblinventaris_id', 'numerical', 'integerOnly'=>true),
			array('tblpengaduan_namapelapor, tblpengaduan_file, tblpengaduan_file_tanggapan', 'length', 'max'=>255),
			array('tblpengaduan_status', 'length', 'max'=>8),
			array('tblpengaduan_keterangan, tblpengaduan_keterangan_tanggapan_proses, tblpengaduan_keterangan_tanggapan_selesai, tblpengaduan_tanggal', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('tblpengaduan_id, tblinventaris_id, tblpengaduan_namapelapor, tblpengaduan_keterangan, tblpengaduan_keterangan_tanggapan_proses, tblpengaduan_keterangan_tanggapan_selesai, tblpengaduan_file, tblpengaduan_file_tanggapan, tblpengaduan_tanggal, tblpengaduan_status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'tblpengaduan_id' => 'Tblpengaduan',
			'tblinventaris_id' => 'Tblinventaris',
			'tblpengaduan_namapelapor' => 'Tblpengaduan Namapelapor',
			'tblpengaduan_keterangan' => 'Tblpengaduan Keterangan',
			'tblpengaduan_keterangan_tanggapan_proses' => 'Tblpengaduan Keterangan Tanggapan Proses',
			'tblpengaduan_keterangan_tanggapan_selesai' => 'Tblpengaduan Keterangan Tanggapan Selesai',
			'tblpengaduan_file' => 'Tblpengaduan File',
			'tblpengaduan_file_tanggapan' => 'Tblpengaduan File Tanggapan',
			'tblpengaduan_tanggal' => 'Tblpengaduan Tanggal',
			'tblpengaduan_status' => 'Tblpengaduan Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('tblpengaduan_id',$this->tblpengaduan_id);
		$criteria->compare('tblinventaris_id',$this->tblinventaris_id);
		$criteria->compare('tblpengaduan_namapelapor',$this->tblpengaduan_namapelapor,true);
		$criteria->compare('tblpengaduan_keterangan',$this->tblpengaduan_keterangan,true);
		$criteria->compare('tblpengaduan_keterangan_tanggapan_proses',$this->tblpengaduan_keterangan_tanggapan_proses,true);
		$criteria->compare('tblpengaduan_keterangan_tanggapan_selesai',$this->tblpengaduan_keterangan_tanggapan_selesai,true);
		$criteria->compare('tblpengaduan_file',$this->tblpengaduan_file,true);
		$criteria->compare('tblpengaduan_file_tanggapan',$this->tblpengaduan_file_tanggapan,true);
		$criteria->compare('tblpengaduan_tanggal',$this->tblpengaduan_tanggal,true);
		$criteria->compare('tblpengaduan_status',$this->tblpengaduan_status,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Tblpengaduan the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}

<?php

/**
 * This is the model class for table "reflingkungan".
 *
 * The followings are the available columns in table 'reflingkungan':
 * @property integer $reflingkungan_id
 * @property integer $reflingkungan_urutan
 * @property string $reflingkungan_nama
 */
class Reflingkungan extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'reflingkungan';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('reflingkungan_urutan', 'numerical', 'integerOnly'=>true),
			array('reflingkungan_nama', 'length', 'max'=>45),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('reflingkungan_id, reflingkungan_urutan, reflingkungan_nama', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'reflingkungan_id' => 'Reflingkungan',
			'reflingkungan_urutan' => 'Reflingkungan Urutan',
			'reflingkungan_nama' => 'Reflingkungan Nama',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('reflingkungan_id',$this->reflingkungan_id);
		$criteria->compare('reflingkungan_urutan',$this->reflingkungan_urutan);
		$criteria->compare('reflingkungan_nama',$this->reflingkungan_nama,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Reflingkungan the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
